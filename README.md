# MEAN stack makes use of MongoDB, Express.js, Angular.js, and Node.js

## Chosen Technologies
To build this app, I've used a bundle of new technologies to improve the development:

    * NodeJS
    * Express
    * Express-session
    * Mongoose
    * Bower
    * Grunt and following helpers:
        - grunt
        - grunt-contrib-compass
        - grunt-contrib-jshint
        - grunt-contrib-uglify
        - grunt-contrib-watch
        - grunt-html2js
        - grunt-jsbeautifier
    * SASS
    * AngularJS
    
### SETTING UP
To setup the environment and run the application, make sure you have Bower, Grunt, Node installed.
Neither bower components nor node_modules have been committed, you will need to run to retrive the required components.

To install or update node modules you will need to run:
```
$ npm install
```
Installing all vendors used in this app, run:
```
$ bower install
```

Finally to build your html application, run:
```
$ grunt
```

After that, open the following link in your favorite browser:
```
http://localhost:5000
```

