( function() {
    angular.module( 'mcFadyen', [
        'ui.router',
        'mcFadyen.partials.tpls',
        'mcFadyen.config',
        'mcFadyen.base',
        'mcFadyen.browse',
        'mcFadyen.checkout',
        'mcFadyen.product',
        'mcFadyen.common'
    ] );
}() );
