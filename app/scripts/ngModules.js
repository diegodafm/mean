( function() {

    'use strict';
    angular.module( 'mcFadyen.config', [] );
    angular.module( 'mcFadyen.common', [] );
    angular.module( 'mcFadyen.base', [] );
    angular.module( 'mcFadyen.product', [] );
    angular.module( 'mcFadyen.browse', [] );
    angular.module( 'mcFadyen.checkout', [] );

}() );
