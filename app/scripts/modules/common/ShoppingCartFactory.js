( function() {

    angular.module( 'mcFadyen.common' ).factory( 'ShoppingCartFactory', ShoppingCartFactory );

    ShoppingCartFactory.$inject = [ 'API_URL', '$http', '$q' ];

    function ShoppingCartFactory( API_URL, $http, $q ) {

        var factory = {

            findAll: function() {
                var def = $q.defer(),
                    url = API_URL + 'shoppingcart';

                $http( {
                        url: url,
                        method: 'GET'
                    } )
                    .success( function( response ) {
                        def.resolve( response );
                    } )
                    .error( function( response ) {
                        def.reject( response );
                    } );
                return def.promise;
            },

            add: function( data ) {
                var def = $q.defer(),
                    url = API_URL + 'shoppingcart/items';

                $http( {
                        url: url,
                        method: 'POST',
                        data: data
                    } )
                    .success( function( response ) {
                        def.resolve( response );
                    } )
                    .error( function( response ) {
                        def.reject( response );
                    } );
                return def.promise;
            },

            delete: function( commerceItemId ) {
                var def = $q.defer(),
                    url = API_URL + 'shoppingcart/items/' + commerceItemId;

                $http( {
                        url: url,
                        method: 'DELETE'
                    } )
                    .success( function( response ) {
                        def.resolve( response );
                    } )
                    .error( function( response ) {
                        def.reject( response );
                    } );
                return def.promise;
            }
        };

        return factory;
    }


} )();
