( function() {

    angular.module( 'mcFadyen.common' ).factory( 'ProductFactory', ProductFactory );

    ProductFactory.$inject = [ 'API_URL', '$http', '$q' ];

    function ProductFactory( API_URL, $http, $q ) {

        var factory = {
            findAll: function() {
                var def = $q.defer(),
                    url = API_URL + 'products';

                $http( {
                        url: url,
                        method: 'GET'
                    } )
                    .success( function( response ) {
                        def.resolve( response );
                    } )
                    .error( function( response ) {
                        def.reject( response );
                    } );
                return def.promise;
            }
        };

        return factory;
    }

} )();
