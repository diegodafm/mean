( function() {
    'use strict';

    BaseController.$inject = [ '$scope', 'ShoppingCartFactory' ];

    angular.module( 'mcFadyen.base' ).controller( 'BaseController', BaseController );

    function BaseController( $scope, ShoppingCartFactory ) {

        var _self = this;

        _self.loadShoppingCart = function() {
            ShoppingCartFactory.findAll( function( response ) {
                if ( response.success ) {
                    console.log( response.data );
                }
            } );
        };

        function init() {
            _self.loadShoppingCart();
        }
        init();

    }

} )();
