( function() {
    'use strict';

    angular.module( 'mcFadyen.checkout' ).controller( 'CheckoutController', CheckoutController );

    CheckoutController.$inject = [ '$scope', 'ShoppingCartFactory' ];

    function CheckoutController( $scope, ShoppingCartFactory ) {

        var _self = this;

        _self.loadShoppingCartItems = function() {

            ShoppingCartFactory.findAll().then( function( response ) {
                if ( response.success ) {
                    _self.shoppingCart = response.data;
                }
            }, function( data ) {
                console.log( 'data', data );
            } );
        };

        _self.removeItem = function( commerceItemId ) {

            ShoppingCartFactory.delete( commerceItemId ).then( function( response ) {
                if ( response.success ) {
                    _self.loadShoppingCartItems();
                }
            }, function( data ) {
                console.log( 'data', data );
            } );
        };

        function init() {
            _self.loadShoppingCartItems();
        }

        init();
    }

} )();
