( function() {
    'use strict';

    angular.module( 'mcFadyen.browse' ).controller( 'BrowseController', BrowseController );

    BrowseController.$inject = [ '$scope', 'ProductFactory' ];

    function BrowseController( $scope, ProductFactory ) {

        var _self = this;

        _self.loadProducts = function() {

            ProductFactory.findAll().then( function( response ) {
                if ( response.success ) {
                    _self.products = response.data;
                }
            }, function( data ) {
                console.log( 'data', data );
            } );

        };

        function init() {
            _self.loadProducts();
        }

        init();
    }

} )();
