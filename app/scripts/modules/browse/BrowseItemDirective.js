( function() {

    'use strict';

    var BrowseItem = function( $templateCache ) {
        return {
            retrict: 'E',
            replace: true,
            template: $templateCache.get( 'modules/browse/_browseItem.html' ),
            controller: 'BrowseItemController',
            controllerAs: 'browseItemCtrl',
            scope: {
                product: '=product'
            }
        };
    };

    BrowseItem.$inject = [ '$templateCache' ];

    angular.module( 'mcFadyen.browse' ).directive( 'browseItem', BrowseItem );
} )();
