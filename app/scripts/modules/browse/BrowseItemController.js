( function() {
    'use strict';

    angular.module( 'mcFadyen.browse' ).controller( 'BrowseItemController', BrowseItemController );

    BrowseItemController.$inject = [ '$scope', 'ShoppingCartFactory' ];

    function BrowseItemController( $scope, ShoppingCartFactory ) {


        $scope.addToSC = function() {
            var data = {
                product_id: $scope.product._id,
                quantity: $scope.quantity,
            };

            ShoppingCartFactory.add( data ).then( function( response ) {
                if ( response.success ) {
                    $scope.$emit( 'CommerceItem:add', response.data );
                    $scope.shopping = false;
                    delete $scope.quantity;
                }
            }, function( response ) {
                console.log( 'data', response );
            } );
        };

        function init() {
            console.log( '$scope', $scope.product );
        }

        init();
    }

} )();
