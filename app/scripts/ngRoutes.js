( function() {

    angular.module( 'mcFadyen' )
        .config( function config( $stateProvider, $urlRouterProvider ) {

            $stateProvider
                .state( 'auth', {
                    url: '/',
                    templateProvider: function( $templateCache ) {
                        return $templateCache.get( 'modules/auth/_auth.html' );
                    },
                    controller: 'AuthController',
                    controllerAs: 'authCtrl',
                    data: {
                        pageTitle: 'Login',
                        authorizedRoles: []
                    }
                } )
                .state( 'root', {
                    url: '',
                    abstract: true,
                    templateProvider: function( $templateCache ) {
                        return $templateCache.get( 'modules/base/_base.html' );
                    },
                    controller: 'BaseController',
                    controllerAs: 'baseCtrl',
                } )
                .state( 'root.shopping', {
                    url: '/shoppingCart',
                    templateProvider: function( $templateCache ) {
                        return $templateCache.get( 'modules/browse/_browse.html' );
                    },
                    controller: 'BrowseController',
                    controllerAs: 'browseCtrl'
                } )
                .state( 'root.checkout', {
                    url: '/checkout',
                    templateProvider: function( $templateCache ) {
                        return $templateCache.get( 'modules/checkout/_checkout.html' );
                    },
                    controller: 'CheckoutController',
                    controllerAs: 'checkoutCtrl'
                } );

            $urlRouterProvider.when( '', '/shoppingCart' );
            $urlRouterProvider.otherwise( function( $injector ) {
                var state = $injector.get( '$state' );
                state.transitionTo( 'root.shopping' );
            } );

        } ).run( function( $rootScope ) {
            $rootScope.$on( '$stateChangeStart', function() {

            } );
        } );
}() );
