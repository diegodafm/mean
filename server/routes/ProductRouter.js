'use strict';

var productController 	= require('../controllers/ProductController'),
	express        		= require('express'),
	router         		= express.Router();

router.route('/')
  .get(productController.findAll());

router.route('/')
  .post(productController.add());

module.exports = router;
