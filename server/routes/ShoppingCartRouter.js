'use strict';

var shoppingCartController 	= require('../controllers/ShoppingCartController'),
	commerceItemController 	= require('../controllers/CommerceItemController'),
	express        			= require('express'),
	router         			= express.Router();

router.route('/')
	.get(shoppingCartController.findByToken());

router.route('/items')
  .post(commerceItemController.add());

router.route('/items/:id')
  .delete(commerceItemController.delete());

module.exports = router;
