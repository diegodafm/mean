'use strict';

var Controller      = require('./controller'),
    ShoppingCartDAO = require('../persistence/ShoppingCartDAO'),
    CommerceItemDAO = require('../persistence/CommerceItemDAO'),
    ProductDAO      = require('../persistence/ProductDAO'),
    logger          = require('../helpers/logger');

class CommerceItemController extends Controller {
  constructor () {
    super('CommerceItemController:');
  }

  findAll() {
    return (request, response) => {

      logger.info('request.session', request.session);

      CommerceItemDAO.findAll(request.session)
        .then(this._success(response))
        .catch(this._error(response));
    };
  }

  add() {
    return (request, response) => {


      let newCommerceItem = {
          product_id: request.body.product_id, 
          quantity: request.body.quantity
        },
        token = request.session.id;

      ProductDAO.findById( newCommerceItem.product_id )
        .then( ( product ) => {

          newCommerceItem.amount = newCommerceItem.quantity * product.price;
          CommerceItemDAO.add( newCommerceItem )
            .then( ( commerceItem ) => {

              logger.info(`Adding commerceItem id ${commerceItem._id} into shoppingCart`);

              ShoppingCartDAO.findAndAdd( token, commerceItem )
                .then( this._success( response ) )
                .catch( this._error( response ) );
            } )              
            .catch( this._error( response ) );
        })
        .catch(this._error(response));
    };
  }

  delete () {
    return (request, response) => {

      var token = request.session.id,
          commerceItemId = request.params.id;

      logger.info('commerceItemId', commerceItemId);

      CommerceItemDAO.findById( commerceItemId )
        .then( ( commerceItem ) => {

          ShoppingCartDAO.findAndRemove ( token, commerceItem )
            .then(this._success(response))
            .catch(this._error(response));

        })
        .catch( this._error(response) );
    };
  }
}

module.exports = new CommerceItemController();
