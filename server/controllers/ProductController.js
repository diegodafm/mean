'use strict';

var Controller = require('./controller'),
    productDAO = require('../persistence/ProductDAO'),
    logger = require('../helpers/logger');


class ProductController extends Controller {
  constructor () {
    super('ProductController:');
  }

  findAll() {
    return (request, response) => {

      logger.info('request.session', request.session.id);

      productDAO.findAll()
        .then(this._success(response))
        .catch(this._error(response));
    };
  }

  findById() {
    return (request, response) => {
      productDAO.findById()
        .then(this._success(response))
        .catch(this._error(response));
    };
  }

  add() {
    return (request, response) => {
      productDAO.add( request.body )
        .then( this._success( response ) )
        .catch( this._error( response ) );
    };
  }

}

module.exports = new ProductController();
