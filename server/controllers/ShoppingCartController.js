'use strict';

var Controller      = require('./controller'),
    ShoppingCartDAO = require('../persistence/ShoppingCartDAO'),
    logger          = require('../helpers/logger');


class ShoppingCartController extends Controller {
  constructor () {
    super('ShoppingCartController:');
  }

  findByToken() {
    return (request, response) => {
      var token = request.session.id;

      ShoppingCartDAO.findByToken( token )
        .then(this._success(response))
        .catch(this._error(response));
    };
  }
}

module.exports = new ShoppingCartController();
