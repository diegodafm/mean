'use strict';

var logger = require('../helpers/logger');

class Controller {
  constructor(loggerPrefix) {
    this._loggerPrefix = loggerPrefix;
    this._logger = logger;
  }

  _error (response) {
    return (err) => {
      this._logger.error(this._loggerPrefix, err);
      response.status(500).json({ success: false, error: err });
    };
  }

  _success (response) {
    return (data) => {
      response.json({ success: true, data: data });
    };
  }
}

module.exports = Controller;
