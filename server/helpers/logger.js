'use strict';

var winston = require('winston');
var morgan  = require('morgan');
var config  = require('../config/config');

class Logger {
  constructor (){
    winston.emitErrs = true;

    this._logger = new winston.Logger({
      transports: [
        new winston.transports.Console({
          level: config.logLevel,
          handleExceptions: true,
          json: false,
          colorize: true,
          timestamp: function() {
            return (new Date()).toISOString();
          },
          humanReadableUnhandledException: true
        })
      ],
      exitOnError: false
    });

    //Here we are saying to morgan to use winston to log on the console, using 'debbug' level
    this._morganLogger = morgan('dev', { 'stream': {
      write: (message, encoding) => {
        //Morgan uses process.stdout for printing as default, therefore it adds a new line 
        //after each message. We are removing it so the logs can be cleaner
        this._logger.debug(message.replace(/\r?\n|\r/g, ''));
      }
    }});
  }

  logError (msg, error) {
    this._logger.info(msg);
    if (error) {
      this._logger.error(error.message);
    }
  }

  get morganLogger () {
    return this._morganLogger;
  }

  set morganLogger (morganLogger) {
    this._morganLogger = morganLogger;
  }

  set console (logger) {
    this._logger = logger;
  }

  get console () {
    return this._logger;
  }

  get error () {
    return this._logger.error;
  }

  get warn () {
    return this._logger.warn;
  }

  get info() {
    return this._logger.info;
  }

  get verbose() {
    return this._logger.verbose;
  }

  get debug() {
    return this._logger.debug;
  }

  get silly() {
    return this._logger.silly;
  }
}

module.exports = new Logger();
