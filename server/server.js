var express  			= require('express'),
	expressSession 		= require('express-session'),
	cookieParser 		= require('cookie-parser'),
	bodyParser 			= require('body-parser'),
	config				= require('./config/config'),
	logger				= require('./helpers/logger');
	mongoose   			= require('mongoose'),
	app 				= express(),
	productRouting		= require('./routes/ProductRouter'),
	shoppingCartRouting	= require('./routes/ShoppingCartRouter'),
	mongoDBconnection	= '';


logger.debug( 'Express:','Setting up cookieParser' );
app.use(cookieParser());

logger.debug( 'Express:','Setting up session' );
app.use( expressSession( config.session ) );

logger.debug( 'Express:','Setting up bodyParser' );
app.use(bodyParser.json());

logger.debug( 'Express:','Setting up bodyParser.urlencoded' );
app.use(bodyParser.urlencoded({ extended: false }))

logger.debug( 'MONGODB: ','Connecting into mongo' );
mongoDBconnection = mongoose.connect(config.mongo.uri, config.mongo.options).connection;
mongoDBconnection.on('error', function(err) { 
	logger.error('MONGODB: ',err.message); 
});
mongoDBconnection.once('open', function() {
  logger.info('MONGODB: ','connected');
});

logger.debug('Express: Setting up endpoints as \'/api/v1/\'');

logger.info('Express: Product endpoint as \'/api/v1/products\'');
app.use('/api/v1/products/', productRouting);

logger.info('Express: ShoppingCart endpoint as \'/api/v1/shoppingcart\'');
app.use('/api/v1/shoppingcart/', shoppingCartRouting);

app.use(express.static('public'));

app.listen( config.port, () => {
	logger.info( 'Express:', 'server listening on port ' + config.port );
});