module.exports = {
  port: process.env.PORT || 5000,
  session : { 
    secret: 'mcFadyenShoppingCart', 
    cookie: { 
      maxAge: new Date(Date.now() + 7200000) 
    },
    resave: true, 
    saveUninitialized: true  
  },
  logLevel: process.env.TEST_LOG_LEVEL || 'silly',
  mongo: {
    uri: 'mongodb://default:8888@ds013901.mlab.com:13901/mcfadyen',
    options: {
      db: {
        safe: true,
        native_parser: true 
      },
      server: {
        socketOptions: {
          keepAlive: 300000, 
          connectTimeoutMS: 30000 
        } 
      }
    }
  },
  paths : {
      dev: {
          root  : 'app/',
          js    : 'app/scripts/',
          sass  : 'app/scss/',
          vendor: 'app/vendor/'
      },
      build: {
          root  : 'public/',
          css   : 'public/css/',
          js    : 'public/js/',
      }
  }
};