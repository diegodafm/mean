'use strict';

var logger = require('../helpers/logger');
var CommerceItem   = require('../models/CommerceItem');

class CommerceItemDAO {

  constructor () {
    this._loggerPrefix = 'CommerceItemDAO:';
  }

  findAll () {
    logger.debug(`${this._loggerPrefix} Constructing findAll promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing findAll promise`);

      CommerceItem.find({}, (error, commerceItems) => {
          if (error) {
            logger.logError(`${this._loggerPrefix} Could not find commerceItems`, error);

            reject(error);
          } else {
            logger.info(`${this._loggerPrefix} returning ${commerceItems.length}`);

            fulfill(commerceItems);
          }
      });
    });
  }

  findById ( id ) {
    logger.debug(`${this._loggerPrefix} Constructing findById promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing findById promise`);

      CommerceItem.findById(id, function (error, commerceItem) {
        if(error) {
          logger.logError(`${this._loggerPrefix} Could not find commerceItem #${id}`, error);

          reject(error);
        } else {
          logger.info(`${this._loggerPrefix} returning commerceItem #${id}`);

          fulfill(commerceItem);
        }
      });
    });
  }

  add (commerceItem ) {
    logger.debug(`${this._loggerPrefix} Constructing add promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing add promise`);
      let commerceItemInstance = new CommerceItem(commerceItem)
      commerceItemInstance.save( {}, function (error, commerceItem) {
        if(error) {
          logger.logError(`Could not add the commerceItem `, error);
          reject(error);
        } else {
          logger.info(`Returning commerceItem #${commerceItem._id}`);

          fulfill(commerceItem);
        }
      });
    });

  }

}

module.exports = new CommerceItemDAO();
