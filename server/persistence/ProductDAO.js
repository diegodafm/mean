'use strict';

var logger = require('../helpers/logger');
var Product   = require('../models/Product');

class ProductDAO {

  constructor () {
    this._loggerPrefix = 'ProductDAO:';
  }

  findAll () {
    logger.debug(`${this._loggerPrefix} Constructing findAll promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing findAll promise`);

      Product.find({}, (error, products) => {
          if (error) {
            logger.logError(`${this._loggerPrefix} Could not find products`, error);

            reject(error);
          } else {
            logger.info(`${this._loggerPrefix} returning ${products.length}`);

            fulfill(products);
          }
      });
    });
  }

  findById (id) {
    logger.debug(`${this._loggerPrefix} Constructing findById promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing findById promise`);

      Product.findById(id, function (error, product) {
        if(error) {
          logger.logError(`${this._loggerPrefix} Could not find product #${id}`, error);

          reject(error);
        } else {
          logger.info(`${this._loggerPrefix} returning product #${id}`);

          fulfill(product);
        }
      });
    });
  }

  add (product ) {
    logger.debug(`${this._loggerPrefix} Constructing add promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing add promise`);
      let productInstance = new Product(product)
      productInstance.save( {}, function (error, product) {
        if(error) {
          logger.logError(`Could not add the product `, error);
          reject(error);
        } else {
          logger.info(`Returning product #${product._id}`);

          fulfill(product);
        }
      });
    });

  }

}

module.exports = new ProductDAO();
