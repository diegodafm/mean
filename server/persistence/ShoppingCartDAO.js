'use strict';

var logger        = require('../helpers/logger'),
    ShoppingCart  = require('../models/ShoppingCart'),
    CommerceItem  = require('../models/CommerceItem'),
    Product       = require('../models/Product');

class ShoppingCartDAO {

  constructor () {
    this._loggerPrefix = 'ShoppingCartDAO:';
  }

  findByToken ( token ) {
    logger.debug(`${this._loggerPrefix} Constructing findAll promise`);

    return new Promise((fulfill, reject) => {
      logger.debug(`${this._loggerPrefix} Executing findAll promise`);

      ShoppingCart.findOne( { token: token } )
        .populate({
          path: 'commerceItem',
          populate: {
            path: 'product_id',
            model: 'Product'
          }
        })
        .exec( (error, shoppingCart) => {
          if(error) {
            logger.logError(`${this._loggerPrefix} Could not find ShoppingCart`, error);
            reject(error);
          } else {
            logger.info(`${this._loggerPrefix} Retrieving ShoppingCart`);
            fulfill(shoppingCart);
          } 
        });
    });
  }

  findAndAdd (token, commerceItem ){

    logger.debug(`finding ShoppingCart token: ${token} and add commerceItem: ${commerceItem._id}` );

    return new Promise((fulfill, reject) => {

      logger.debug(`${this._loggerPrefix} Executing findAndUpdate promise`);

      ShoppingCart.findOneAndUpdate( { 
          token: token 
        },{ 
          $push: { 
            commerceItem: commerceItem 
          },
          $inc: { 
            amount : commerceItem.amount
          }
        }, {
          new: true,
          upsert: true
        }, (error, shoppingCart) => {
          if (error) {
            logger.logError(`${this._loggerPrefix} Could not find ShoppingCart`, error);
            reject(error);
          } else {
            logger.info(`${this._loggerPrefix} returning ShoppingCart id: ${shoppingCart._id}`);
            fulfill(shoppingCart);
          }
      });
    } );
  }

  findAndRemove (token, commerceItem ){

    logger.debug(`finding ShoppingCart token: ${token} and remove commerceItem: ${commerceItem._id}` );
    
    return new Promise((fulfill, reject) => {

      logger.debug(`${this._loggerPrefix} Executing findAndUpdate promise`);

      ShoppingCart.findOneAndUpdate( { 
          token: token 
        },{ 
          $pull: { 
            commerceItem: commerceItem._id
          },
          $inc: { 
            amount : ( commerceItem.amount * -1 )
          }
        }, {
          new: true,
          upsert: true
        }, (error, shoppingCart) => {
          if (error) {
            logger.logError(`${this._loggerPrefix} Could not find ShoppingCart`, error);
            reject(error);
          } else {
            logger.info(`${this._loggerPrefix} returning ShoppingCart id: ${shoppingCart._id}`);
            fulfill(shoppingCart);
          }
      });
    });
  }

}

module.exports = new ShoppingCartDAO();
