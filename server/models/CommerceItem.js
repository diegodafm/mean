'use strict';

var mongoose = require('mongoose'),
	Schema   = mongoose.Schema;

var commerceItem = new Schema({
  product_id: {
  	type: Schema.ObjectId, 
  	ref: 'Product'
  },
  quantity: {
  	type: Number, 
  	required: true
  },
  amount: {
  	type: Number, 
  	required: true
  }
});

module.exports = mongoose.model('CommerceItem', commerceItem, 'CommerceItem');