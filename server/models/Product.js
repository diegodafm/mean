'use strict';

var mongoose = require('mongoose'),
	Schema   = mongoose.Schema,

	Product = new Schema({
		name: {type: String, default: ''},
  		image: {type: String, default: ''},
  		price: {type: Number, default: 0, min: 0}
	});

module.exports = mongoose.model('Product', Product, 'Product');