'use strict';

var mongoose = require('mongoose'),
	Schema   = mongoose.Schema;

var shoppingCart = new Schema({
  commerceItem: [ { 
  	type: Schema.ObjectId,
  	ref: 'CommerceItem', 
  	required: true 
  }],
  amount: {
  	type: Number, 
  	required: true
  },
  token: {
  	type: String,
  	required: true
  }
});

module.exports = mongoose.model('ShoppingCart', shoppingCart, 'ShoppingCart');