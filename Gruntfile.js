module.exports = function ( grunt ) {

    'use strict';

    var config = require('./server/config/config');

    grunt.initConfig({
        jsbeautifier : {
            files : [
                config.paths.dev.js + '**/*.js',
                config.paths.dev.js + '**/*.html',
                config.paths.dev.js + '**/*.html'
            ],
            options : {
                js: {
                    spaceInParen: true
                },
                html: {
                    braceStyle: 'collapse',
                    indentChar: ' ',
                    indentScripts: 'keep',
                    indentSize: 4,
                    maxPreserveNewlines: 10,
                    preserveNewlines: true,
                    unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u'],
                    wrapLineLength: 0
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            js: {
                files: [
                    'Gruntfile.js',
                    config.paths.dev.js + '**/*.js'
                ],
                tasks: ['jsbeautifier', 'jshint', 'uglify']
            },
            css: {
                files: [ config.paths.dev.sass + '*'],
                tasks: ['compass']
            },
            html: {
                files: [
                    'index.html',
                    config.paths.dev.js + '/modules/**/*.html', 
                ],
                tasks: ['jsbeautifier', 'html2js', 'uglify']
            }
        },
        html2js: {
            options: {
                quoteChar: '\'',
                rename: function(moduleName) {
                    return moduleName.replace('../public/partials/', '');
                },
                module: 'mcFadyen.partials.tpls',
                indentString: '    ',
                singleModule: true,
                useStrict: true,
                base: config.paths.dev.js,
                htmlmin: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                }
            },
            main: {
                src: [
                    config.paths.dev.js + '/**/*.html'
                ],
                dest: config.paths.dev.js + 'ngTemplates.js'
            }
        },
        compass: {
            dev: {
                options: {
                    sassDir:config.paths.dev.sass,
                    cssDir: config.paths.build.css
                }
            }
        },
        // clean: {
        //     options: { force: true },
        //     cache: [
        //         '.sass-cache'
        //     ]
        // },
        jshint: {
            all: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: [
                    'Gruntfile.js',
                    config.paths.dev.js + '**/*.js'
                ]
            }
        },
        uglify: {
            options: {
                mangle: false,
                expand: true
            },
            target: {
                files: {
                    'public/js/base.min.js': [
                        config.paths.dev.vendor + 'jquery/dist/jquery.min.js',
                        config.paths.dev.vendor + 'angular/angular.min.js',
                        config.paths.dev.vendor + 'angular-ui-router/release/angular-ui-router.min.js',
                        config.paths.dev.vendor + 'Materialize/dist/js/materialize.min.js',
                    ],
                    'public/js/app.min.js': [
                        config.paths.dev.js + 'ngApp.js',
                        config.paths.dev.js + 'ngRoutes.js',
                        config.paths.dev.js + 'ngConfig.js',
                        config.paths.dev.js + 'ngModules.js',
                        config.paths.dev.js + 'ngTemplates.js',
                        config.paths.dev.js + '**/*js'
                    ]
                }
            }
        }
    });

    // ===========================================================================
    // EVENTS ====================================================================
    // ===========================================================================
    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(('EVENT ' + target + ':').green ,  (filepath + ' has ' + action).yellow);
    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-jsbeautifier');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-html2js');

    // ===========================================================================
    // REGISTER TASKS ============================================================
    // ===========================================================================
    grunt.registerTask( 'build', [ 'jshint', 'uglify', 'compass', 'html2js' ] );
    grunt.registerTask( 'default', [ 'build' ,'watch'] );
    grunt.registerTask( 'tdd', [ 'karma:unit'] );
    // grunt.registerTask( 'clear', [ 'clean:cache'] );
    grunt.registerTask( 'test-continuous', [ 'karma:continuous'] );

};